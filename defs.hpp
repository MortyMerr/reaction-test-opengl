#ifndef REACTION_DEFS_HPP
#define REACTION_DEFS_HPP

#define windowHeight 600
#define windowWidth 760
#define biggerR 300
#define smallerR 150

#define pauseStep 1  //TOUSER fps
#define rIncreaseStep 1

#define lineWidth 3
#endif // !REACTION_DEFS_HPP


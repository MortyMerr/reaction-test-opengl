#ifndef REACTION_PRINTABLE_HPP
#define REACTION_PRINTABLE_HPP

//TOUSER parent for all printable objects
class Printable
{
public:
  virtual ~Printable() = default;
  virtual void print() const = 0;
};

#endif // !REACTION_PRINTABLE_HPP   
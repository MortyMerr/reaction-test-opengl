#ifndef REACTION_CIRCLE_HPP
#define REACTION_CIRCLE_HPP

#include "Printable.hpp"
#include "Point.hpp"

class Circle :
  public Printable
{
public:
  enum class Color {one, another};

  Circle(Color color, Point&& center, double r);

  void print() const override;

  Color getColor() const;
  void move(size_t x, size_t y);
  void move(Point&& point);
  double getR() const;
  void incR(double dr);
public://FIXME
  Point center_;
  double r_;
  Color color_;
};

#endif // !REACTION_CIRCLE_HPP
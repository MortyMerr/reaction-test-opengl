#include <glut.h>
#include "GraphicUtils.hpp"
#include "defs.hpp"

int main(int argc, char * argv[])
{
  glutInit(&argc, argv);			
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
  glutInitWindowSize(windowWidth - 20, windowHeight);
  glutCreateWindow("Nards long");
  glClearColor(0.5, 0.5, 0.5, 0.5);	
  glutMouseFunc(mouseButton);
  glutDisplayFunc(draw);		
  glutReshapeFunc(reshape); 
  glutMainLoop(); 
  return 0;
}
#ifndef REACTION_LOGIC_HPP
#define REACTION_LOGIC_HPP

#include <vector>
#include "Circle.hpp"

extern std::vector <double> statistik; //first - time, second - (Rout - Rin)
void doLogic(Circle& c, int& increase);
void handleClick(double inR, double outR);
extern unsigned int lastEqualMoment;
extern bool pressed;

#endif // !REACTION_LOGIC_HPP

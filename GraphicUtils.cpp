#include "GraphicUtils.hpp"
#include <iostream>
#include <Windows.h>
#include <sstream>
#include <iomanip>
#include "defs.hpp"
#include "Logic.hpp"


void initialize()
{
  glClearColor(0.5, 0.5, 0.5, 0.5);
  glMatrixMode(GL_PROJECTION);
}

void drawText(const std::string& text, int x, int y)
{
  glMatrixMode(GL_PROJECTION);
  double *matrix = new double[16];
  glGetDoublev(GL_PROJECTION_MATRIX, matrix);
  glLoadIdentity();
  gluOrtho2D(0, windowWidth - 20, 0, windowHeight);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glPushMatrix();
  glLoadIdentity();
  glRasterPos2i(x, y);
  for (size_t i = 0; i < text.size(); i++)
  {
    glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int)text[i]);
  }
  glPopMatrix();
}

void reshape(int w, int h)
{
  gluOrtho2D(0, w, 0, h);
}

void draw() {
  glClear(GL_COLOR_BUFFER_BIT);
  circleOut.print();
  circleIn.print();
  if (moving)
  {
    doLogic(circleOut, increase);
    printStat();
  }
  Sleep(pauseStep);
  glutSwapBuffers();
  glutPostRedisplay();
}

void mouseButton(int button, int state, int x, int y)
{
  y = windowHeight - y; //TOREMEMBER ��� �� �������, ��� ����� ������� ������� ��������� �������
  if ((button == GLUT_RIGHT_BUTTON)
    && (state == GLUT_DOWN)
    && !pressed) 
  {
    handleClick(circleIn.getR(), circleOut.getR());
    pressed = true;
  }

  if ((button == GLUT_LEFT_BUTTON)
    && (state == GLUT_DOWN)) {
    if (moving)
    {
      exit(0);
    }
    moving = true;
    lastEqualMoment = clock();
  }
}

void printStat()
{
  int i = statistik.size(), pos = 0;
  const int size = i;
  std::stringstream ss;
  std::string out;
  while ((i-- > 0)
    && (i > (size - 10)))
  {
    ss.str(std::string());
    ss << i + 1
      << " "
      << statistik[i];
    out = ss.str();
    drawText(out, 10, 20 + pos++ * 20);
  }
}

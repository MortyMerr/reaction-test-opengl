#include "Logic.hpp"
#include <ctime>
#include "defs.hpp"

std::vector <double > statistik; 
unsigned int lastEqualMoment;
static int passSmallerCircle = -1;
bool pressed = false;

void doLogic(Circle& c, int& increase)
{
  const auto currentR = c.getR();
  if ((currentR < 2)
    || (currentR > biggerR))
  {
    increase *= -1;
    if (increase == -1) {
      pressed = false;
    }
  }
  if (abs(currentR - smallerR) < rIncreaseStep)
  {
    lastEqualMoment = clock();
    passSmallerCircle *= -1;
  }
  c.incR(increase * rIncreaseStep);
}

void handleClick(double inR, double outR)
{
  double tmp = passSmallerCircle  * static_cast<double>(clock() - lastEqualMoment) / CLOCKS_PER_SEC;
  statistik.push_back(tmp);
}

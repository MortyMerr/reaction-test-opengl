#ifndef REACTION_GRAPHIC_UTILS_HPP
#define REACTION_GRAPHIC_UTILS_HPP
#include <string>
#include <glut.h>
#include <memory>
#include "Circle.hpp"
#include "defs.hpp"

/*___*/
static bool moving = false;
static int increase = 1, missCount = 0;
static Circle circleIn(Circle::Color::one, { windowWidth / 2, windowHeight / 2 }, smallerR),
circleOut(Circle::Color::another, { windowWidth / 2, windowHeight / 2 }, biggerR);
/*___*/

void initialize();
void drawText(const std::string& text , int x, int y);
void reshape(int w, int h);
void draw();
void mouseButton(int button, int state, int x, int y);
void printStat();

#endif // !REACTION_GRAPHIC_UTILS_HPP

#include "Circle.hpp"
#include <glut.h>
#include <utility>
#define _USE_MATH_DEFINES
#include <cmath>
#include "defs.hpp"


void Circle::print() const
{
  if (color_ == Color::one) {
    glColor3f(0.1, 0.1, 0.1);
  } else {
    glColor3f(0.99, 0.99, 0.99);
  }

  glLineWidth(lineWidth);
  glBegin(GL_LINE_STRIP);
  for (double alpha = 0; alpha < 2 * M_PI; alpha += M_PI / 100) {
    glVertex2d(center_.x + cos(alpha) * r_,
      center_.y + sin(alpha) * r_);
  }
  glEnd();

  /*if (color_ == Color::black) {
    glColor3f(0.3, 0.3, 0.3);
  }
  else {
    glColor3f(0.2, 0.2, 0.2);
  }
  glBegin(GL_LINE_STRIP);
  for (double alpha = 0; alpha < 2 * M_PI; alpha += M_PI / 100) {
    glVertex2d(center_.x + cos(alpha) * r_,
      center_.y + sin(alpha) * r_);
  }
  glEnd();*/
}

Circle::Circle(Color color, Point&& center, double r) :
  color_(color),
  center_(std::move(center)),
  r_(r)
{
}

Circle::Color Circle::getColor() const
{
  return color_;
}

void Circle::move(size_t x, size_t y)
{
  center_.x = x;
  center_.y = y;
}

void Circle::move(Point&& point)
{
  center_ = std::move(point);
}

double Circle::getR() const
{
  return r_;
}

void Circle::incR(double dr)
{
  r_ += dr;
}